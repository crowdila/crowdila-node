const { User, Post, Option, Tag, Avatar, Answer } = require('../comms')

const MutationResolvers = {
  async createUser(_, { user }, __) {
    return await User.createUser(user.email, user.username, user.password)
  },
  async updateTags(_, { tags }, context) {
    currentUser = await User.findUserByAuth(context.username, context.token)
    return await User.updateTags(currentUser.id, tags)
  },
  async updateAvatar(_, { avatarId }, context) {
    currentUser = await User.findUserByAuth(context.username, context.token)
    return await User.updateAvatar(currentUser.id, avatarId)
  },
  async createPost(_, { post }, context) {
    currentUser = await User.findUserByAuth(context.username, context.token)
    return await Post.createPost(post.title, post.tags, post.questions, currentUser.id)
  },
  async createAnswer(_, { postId, answer }, context) {
    currentUser = await User.findUserByAuth(context.username, context.token)
    return await Answer.createAnswer(currentUser.id, postId, answer)
  },
  async addOption(_, { questionId, option }, context) {
    currentUser = await User.findUserByAuth(context.username, context.token)
    return await Option.createOption(questionId, option)
  }
}

module.exports = MutationResolvers
