const { User, Post, Tag, Avatar } = require('../comms')

const QueryResolvers = {
  async me(_, __, context) {
    return await User.findUserByAuth(context.username, context.token)
  },
  async post(_, { id }, context) {
    return await Post.findPostById.load(id)
  },
  async searchPosts(_, { title, limit, from }, context) {
    return await Post.findPostByTitle(title, limit, from)
  },
  async filterByTags(_, { tags, limit, from }, context) {
    return await Post.findPostsByTags(tags, limit, from)
  },
  async tags(_, __, context) {
    return await Tag.listTags()
  },
  async login(_, { username, password }, __) {
    return await User.findUserByLogin(username, password)
  },
  async avatars(_, __, context) {
    return await Avatar.listAvatars()
  },
  async feed(_, { limit, from }, context) {
    let currentUser = await User.findUserByAuth(context.username, context.token)
    let tags = await Tag.findTagsByUserId(currentUser.id)
    let filteredTags = tags.map(function(e) { return e.name } );
    return await Post.findPostsByTags(filteredTags, limit, from)
  }
}

module.exports = QueryResolvers
