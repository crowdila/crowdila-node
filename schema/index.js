const UserResolvers = require('./user')
const QuestionResolvers = require('./question')
const PostResolvers = require('./post')
const QueryResolvers = require('./query')
const MutationResolvers = require('./mutation')

module.exports = {
  User: UserResolvers,
  Question: QuestionResolvers,
  Post: PostResolvers,
  Query: QueryResolvers,
  Mutation: MutationResolvers
}
