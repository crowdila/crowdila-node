const { Option } = require('../comms')

const QuestionResolvers = {
  async options(question) {
    return await Option.findOptionsByQuestionId(question.id)
  }
}

module.exports = QuestionResolvers
