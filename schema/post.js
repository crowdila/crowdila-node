const { Question, Tag, User } = require('../comms')

const PostResolvers = {
  async questions(post) {
    return await Question.findQuestionsByPostId(post.id)
  },
  async tags(post) {
    return await Tag.findTagsByPostId(post.id)
  },
  async user(post){
    return await User.findUserById.load(post.userId)
  }
}

module.exports = PostResolvers
