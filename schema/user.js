const { Post, Tag, Avatar } = require('../comms')

const UserResolvers = {
  async posts(user, { limit, from }) {
    return await Post.findPostsByUserId(user.id, limit, from)
  },
  async tags(user) {
    return await Tag.findTagsByUserId(user.id)
  },
  async avatar(user) {
    return await Avatar.findAvatarById.load(user.avatar_id)
  }
}

module.exports = UserResolvers
