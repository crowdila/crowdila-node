const rp = require('request-promise');

const railsHost = process.env.RAILS_HOST || '127.0.0.1';
const railsPort = process.env.RAILS_PORT || 3000;
const basePath = process.env.RALS_BASE_PATH || '/api/internal'

function uriFromPath (path) {
  return `http://${railsHost}:${railsPort}${basePath}${path}`
}

async function get(path, qs) {
  var uri = uriFromPath(path);

  var options = {
    uri,
    body: qs,
    json: true,
    simple: false,
    resolveWithFullResponse: true
  };

  data = await rp.get(options);
  return data;
}

async function post(path, body) {
  var uri = uriFromPath(path);

  var options = {
    uri,
    body,
    json: true,
    simple: false,
    resolveWithFullResponse: true
  };

  data = await rp.post(options);
  return data;
}

module.exports = { get, post }
