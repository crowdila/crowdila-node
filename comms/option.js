const { get, post } = require('./communicator');
const DataLoader = require('dataloader')

function convertBodyToOption(body) {
  if (body === undefined || body === null) {
    return body
  }

  return {
    id: body.id,
    name: body.text,
    votes: body.answer_count || 0
  }
}

async function findOptionsById(ids) {
  let path = `/option/find_by_ids`;
  let { body, statusCode } = await get(path, { ids });

  return body.map(convertBodyToOption);
}

async function findOptionsByQuestionId(questionId) {
  let path = '/option/find_by_question_id'

  let { body, statusCode } = await get(path, { question_id: questionId })

  return body.map(convertBodyToOption);
}

async function createOption(questionId, option) {
  let path = '/option'

  let { body, statusCode } = await post(path, { question_id: questionId, option })

  return convertBodyToOption(body)
}

const findOptionById = new DataLoader(ids => findOptionsById(ids))

module.exports = { findOptionById, findOptionsByQuestionId, createOption }
