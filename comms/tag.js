const { get, post } = require('./communicator');

async function findTagsByUserId(userId) {
  let path = '/tag/find_by_user_id';

  let { body, statusCode } = await get(path, { user_id: userId });

  return body
}

async function listTags() {
  let path = '/tag';
  let { body, statusCode } = await get(path, {});

  return body
}

async function findTagsByPostId(postId) {
  let path = '/tag/find_by_post_id'
  let { body, statusCode } = await get(path, { post_id: postId })

  return body
}

module.exports = { findTagsByUserId, listTags, findTagsByPostId }
