const { get, post } = require('./communicator');
const DataLoader = require('dataloader')

function convertBodyToPost(body) {
  console.log(body)

  if(body === undefined || body === null) {
    return null
  }

  return {
    id: body.id,
    title: body.title,
    createdAt: Date.parse(body.created_at) / 1000,
    questionCount: body.questions_count || 0,
    answerCount: body.answer_count || 0,
    userId: body.user_id
  }
}

function convertBodyToPosts(body) {
  return body.map(convertBodyToPost)
}

async function findPostByTitle(title, limit, from) {
  let path = '/post/find_by_title';
  let { body, statusCode } = await get(path, { title, limit, id_from: from });

  return convertBodyToPosts(body)
}

async function findPostsByTags(tags, limit, from) {
  let path = '/post/find_by_tags';
  let { body, statusCode } = await get(path, { tag_names: tags, limit, id_from: from });

  return convertBodyToPosts(body)
}

async function findPostsByUserId(id, limit, id_from) {
  let path = `/post/find_by_user_id`;
  let { body, statusCode } = await get(path, { user_id: id, limit, id_from });

  return convertBodyToPosts(body)
}

async function findPostsById(ids) {
  let path = '/post/find_by_ids'

  let { body, statusCode } = await get(path, { ids });

  return convertBodyToPosts(body)
}

function postTypeConvert(schemaType) {
  return schemaType.charAt(0).toUpperCase() + schemaType.slice(1).toLowerCase() + 'Question'
}

async function createPost(title, tags, questions, userId) {
  let path = '/post'

  qs = questions.map(function(q) {
    return {
      text: q.title,
      type: postTypeConvert(q.type),
      options: q.options,
      multi_answers: q.multiChoice,
      user_can_add: q.openAnswers
    }
  })

  let { body, statusCode } = await post(path, {
    title,
    tag_names: tags,
    user_id: userId,
    questions: qs
  })


  console.log(statusCode)
  return convertBodyToPost(body)
}

const findPostById = new DataLoader(ids => findPostsById(ids))

module.exports = { findPostByTitle, findPostsByTags, findPostsByUserId, findPostById, createPost }
