const { get, post } = require('./communicator');
const DataLoader = require('dataloader')

function convertBodyToQuestion(body) {
  if (body === undefined || body === null) {
    return null
  }

  return {
    id: body.id,
    title: body.text,
    type: body.type.replace('Question', '').toUpperCase(),
    multiChoice: body.multi_answer,
    openAnswers: body.user_can_add
  }
}

async function findQuestionsById(ids) {
  let path = `/question`;
  let { body, statusCode } = await get(path, { ids });

  return body.map(convertBodyToQuestion)
}

async function findQuestionsByPostId(postId) {
  let path = '/question/find_by_post_id'
  let { body, statusCode } = await get(path, { post_id: postId })

  return body.map(convertBodyToQuestion)
}

const findQuestionById = new DataLoader(ids => findQuestionsById(ids))

module.exports = { findQuestionById, findQuestionsByPostId }
