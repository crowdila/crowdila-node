const { get } = require('./communicator');
const DataLoader = require('dataloader')

async function findAvatarsById(ids) {
  let path = `/avatar/find_by_ids`;
  let { body, statusCode } = await get(path, { ids });

  avatars = body.map(function(avatar) {
    return {
      id: avatar.id,
      url: avatar.avatar_url
    }
  })

  return avatars
}

async function listAvatars() {
  let path = '/avatar'

  let { body, statusCode } = await get(path, {});

  return body.map(function(avatar) {
    return {
      id: avatar.id,
      url: avatar.avatar_url
    }
  })
}

const findAvatarById = new DataLoader(ids => findAvatarsById(ids))

module.exports = { findAvatarById, listAvatars }
