const { post } = require('./communicator');

async function createAnswer(userId, postId, answer) {
  let path = '/answer'

  let qas = answer.map(function(qs) {
    return {
      question_id: qs.questionId,
      option_ids: qs.optionIds,
      text: qs.text
    }
  })
  let { body, statusCode } = await post(path, { user_id: userId, post_id: postId, question_answers: qas });

  return statusCode === 200
}

module.exports = { createAnswer }
