const rp = require('request-promise');

const expect = require('chai').expect
const sinon = require('sinon');
const Promise = require('bluebird')

const { get, post } = require('./communicator')

function stubReqSuccess(sandbox, method, body) {
  var stub = sandbox.stub(rp, method)
  stub.returns(new Promise(function(resolve, reject) {
    resolve({
      statusCode: 200,
      body
    })
  }))
  return stub
}

describe('Communicator module', function () {
  describe('Get', function() {
    it('calls get request', async function () {
      var requestStub = stubReqSuccess(this.sandbox, 'get', { id: 1 });
      var result = await get('/user', { id: 1 })
      sinon.assert.calledWith(requestStub, {
        uri: `http://127.0.0.1:3000/api/internal/user`,
        qs: { id: 1 },
        json: true,
        simple: false,
        resolveWithFullResponse: true
      })

      expect(result.body).to.eql({ id: 1 })
      expect(result.statusCode).to.eql(200)
    })
  })

  describe('Post', function() {
    it('calls post request', async function () {
      var requestStub = stubReqSuccess(this.sandbox, 'post', { id: 1 });
      var result = await post('/user', { id: 1 })
      sinon.assert.calledWith(requestStub, {
        uri: `http://127.0.0.1:3000/api/internal/user`,
        body: { id: 1 },
        json: true,
        simple: false,
        resolveWithFullResponse: true
      })

      expect(result.body).to.eql({ id: 1 })
      expect(result.statusCode).to.eql(200)
    })
  })
})
