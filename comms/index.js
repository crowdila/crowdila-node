const Answer = require('./answer')
const Avatar = require('./avatar')
const Option = require('./option')
const Post = require('./post')
const Question = require('./question')
const Tag = require('./tag')
const User = require('./user')

module.exports = {
  Answer,
  Avatar,
  Option,
  Post,
  Question,
  Tag,
  User
}
