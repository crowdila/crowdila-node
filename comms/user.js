const { get, post } = require('./communicator');
const { AuthenticationError, ApolloError } = require('apollo-server');
const DataLoader = require('dataloader')

async function findUserByAuth(username, token) {
  let path = '/user/auth';
  let { body, statusCode } = await get(path, { username, token });
  if (statusCode == 404) {
    throw new AuthenticationError('Wrong auth');
  }

  return body
}

async function findUserByLogin(username, password) {
  let path = '/user/login';
  let { body, statusCode } = await get(path, { username, password });
  if (statusCode == 404) {
    throw new AuthenticationError('Wrong auth');
  }

  return body
}

async function findUsersById(ids) {
  let path = `/user/find_by_ids`;
  let { body, statusCode } = await get(path, { ids });
  if (statusCode == 404) {
    throw new ApolloError('User not found');
  }

  return body
}

async function createUser(email, username, password) {
  let path = `/user`
  let { body, statusCode } = await post(path, { email, username, password })

  if (statusCode >= 400) {
    throw new ApolloError('User creation failed')
  }

  return body
}

async function updateTags(userId, tags) {
  let path = `/user/update_tags`
  let { body, statusCode } = await post(path, { tag_names: tags, id: userId })

  return body
}

async function updateAvatar(userId, avatarId) {
  let path = '/user/update_avatar'
  let { body, statusCode } = await post(path, { avatar_id: avatarId, id: userId })

  return body
}

const findUserById = new DataLoader(ids => findUsersById(ids))

module.exports = { findUserByAuth, findUserByLogin, findUserById, createUser, updateTags, updateAvatar }
