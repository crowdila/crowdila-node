const rp = require('request-promise');

const chai = require('chai')
const expect = chai.expect
chai.use(require('chai-as-promised'))
const sinon = require('sinon');
const Promise = require('bluebird');

const { AuthenticationError, ApolloError } = require('apollo-server');

const User = require('./user')

function stubReq(sandbox, method, statusCode, body) {
  var stub = sandbox.stub(rp, method)
  stub.returns(new Promise(function(resolve, reject) {
    resolve({
      statusCode,
      body
    })
  }))
  return stub
}

describe('User model', function () {
  describe('findUserByAuth', function () {
    describe('Success', function() {
      it('finds user', async function () {
        var requestStub = stubReq(this.sandbox, 'get', 200, {
          id: 10,
          email: 'messam@sth.com'
        });

        var data = await User.findUserByAuth('messam', 'habalola');

        sinon.assert.calledWith(requestStub, {
          uri: `http://127.0.0.1:3000/api/internal/user/auth`,
          qs: {
            username: 'messam',
            token: 'habalola'
          },
          json: true,
          simple: false,
          resolveWithFullResponse: true
        })

        expect(data).to.eql({
          id: 10,
          email: 'messam@sth.com'
        })
      })
    })

    describe('Failure', function(){
      it('finds user', async function () {
        var requestStub = stubReq(this.sandbox, 'get', 404, {});
        await expect(User.findUserByAuth('messam', 'habalola')).to.be.rejectedWith(AuthenticationError)
      })
    })
  })
  describe('findUserByLogin', function () {
    describe('Success', function() {
      it('finds user', async function () {
        var requestStub = stubReq(this.sandbox, 'get', 200, {
          id: 10,
          email: 'messam@sth.com'
        });

        var data = await User.findUserByLogin('messam', 'habalola');

        sinon.assert.calledWith(requestStub, {
          uri: `http://127.0.0.1:3000/api/internal/user/login`,
          qs: {
            username: 'messam',
            password: 'habalola'
          },
          json: true,
          simple: false,
          resolveWithFullResponse: true
        })

        expect(data).to.eql({
          id: 10,
          email: 'messam@sth.com'
        })
      })
    })

    describe('Failure', function(){
      it('finds user', async function () {
        var requestStub = stubReq(this.sandbox, 'get', 404, {});
        await expect(User.findUserByLogin('messam', 'habalola')).to.be.rejectedWith(AuthenticationError)
      })
    })
  })
})
