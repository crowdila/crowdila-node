const rp = require('request-promise');

const expect = require('chai').expect
const sinon = require('sinon');
const Promise = require('bluebird')

describe('Mutation Object', function () {
  describe('createUser', function() {
    it('creates and returns user', async function () {
      var stub = this.sandbox.stub(rp, 'post')
      let postOptions = undefined

      stub.callsFake(function(options) {
        postOptions = options
        return new Promise(function(resolve, reject) {
          resolve({
            statusCode: 200,
            body: {
              id: 20,
              email: 'messam@this.com',
              username: 'messam',
              token: 'abcdef'
            }
          })
        })
      })

      let data = await rp({
        method: 'POST',
        body: {
          query: 'mutation {createUser(user: {email: "messam@this.com", username: "messam", password: "nyancat"}) {email,username,token}}'
        },
        uri: 'http://localhost:4000',
        json: true,
        simple: true,
        headers: {
          'Content-Type': 'application/json'
        }
      })

      expect(postOptions.body.username).to.eql('messam')
      expect(postOptions.body.email).to.eql('messam@this.com')
      expect(postOptions.body.password).to.eql('nyancat')

      expect(data.data.createUser.email).to.eql('messam@this.com')
      expect(data.data.createUser.username).to.eql('messam')
      expect(data.data.createUser.token).to.eql('abcdef')
    })
  })

  describe('updateTags', function() {
    it('updates user tags and returns user', async function () {
      var postStub = this.sandbox.stub(rp, 'post')
      let postOptions = undefined
      let getOptions = undefined

      postStub.callsFake(function(options) {
        postOptions = options
        return new Promise(function(resolve, reject) {
          resolve({
            statusCode: 200,
            body: {
              id: 20,
              email: 'messam@this.com',
              username: 'messam',
              token: 'abcdef'
            }
          })
        })
      })

      var getStub = this.sandbox.stub(rp, 'get')

      getStub.callsFake(function(options) {
        getOptions = options
        return new Promise(function(resolve, reject) {
          resolve({
            statusCode: 200,
            body: {
              id: 20,
              email: 'messam@this.com',
              username: 'messam',
              token: 'abcdef'
            }
          })
        })
      })

      let data = await rp({
        method: 'POST',
        body: {
          query: 'mutation {updateTags(tags: ["habalola", "2olla"]) {email,username,token}}'
        },
        uri: 'http://localhost:4000',
        json: true,
        simple: true,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Token token="abcdef",username="messam"'
        }
      })

      expect(postOptions.body.tag_names).to.eql(['habalola', '2olla'])
      expect(postOptions.body.id).to.eql(20)

      expect(getOptions.qs.username).to.eql('messam')
      expect(getOptions.qs.token).to.eql('abcdef')

      expect(data.data.updateTags.email).to.eql('messam@this.com')
      expect(data.data.updateTags.username).to.eql('messam')
      expect(data.data.updateTags.token).to.eql('abcdef')
    })
  })

  describe('updateAvatar', function() {
    it('updates user avatar and returns user', async function () {
      var postStub = this.sandbox.stub(rp, 'post')
      let postOptions = undefined
      let getOptions = undefined

      postStub.callsFake(function(options) {
        postOptions = options
        return new Promise(function(resolve, reject) {
          resolve({
            statusCode: 200,
            body: {
              id: 20,
              email: 'messam@this.com',
              username: 'messam',
              token: 'abcdef'
            }
          })
        })
      })

      var getStub = this.sandbox.stub(rp, 'get')

      getStub.callsFake(function(options) {
        getOptions = options
        return new Promise(function(resolve, reject) {
          resolve({
            statusCode: 200,
            body: {
              id: 20,
              email: 'messam@this.com',
              username: 'messam',
              token: 'abcdef'
            }
          })
        })
      })

      let data = await rp({
        method: 'POST',
        body: {
          query: 'mutation {updateAvatar(avatarId: 5) {email,username,token}}'
        },
        uri: 'http://localhost:4000',
        json: true,
        simple: true,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Token token="abcdef",username="messam"'
        }
      })

      expect(postOptions.body.avatar_id).to.eql(5)
      expect(postOptions.body.id).to.eql(20)

      expect(getOptions.qs.username).to.eql('messam')
      expect(getOptions.qs.token).to.eql('abcdef')

      expect(data.data.updateAvatar.email).to.eql('messam@this.com')
      expect(data.data.updateAvatar.username).to.eql('messam')
      expect(data.data.updateAvatar.token).to.eql('abcdef')
    })
  })

  describe('createPost', function() {
    it('sends post creation correctly', async function () {
      var postStub = this.sandbox.stub(rp, 'post')
      let postOptions = undefined
      let getOptions = undefined

      postStub.callsFake(function(options) {
        postOptions = options
        return new Promise(function(resolve, reject) {
          resolve({
            statusCode: 200,
            body: {
              id: 20,
              email: 'messam@this.com',
              username: 'messam',
              token: 'abcdef'
            }
          })
        })
      })

      var getStub = this.sandbox.stub(rp, 'get')

      getStub.callsFake(function(options) {
        getOptions = options
        return new Promise(function(resolve, reject) {
          resolve({
            statusCode: 200,
            body: {
              id: 20,
              email: 'messam@this.com',
              username: 'messam',
              token: 'abcdef'
            }
          })
        })
      })

      mutation = `
        mutation {
          createPost(post: {
            title: "magic",
            tags: ["habalola", "2olla"],
            questions: [
              {
                title: "Q1",
                type: MCQ,
                options: ["A1", "A2"],
                multiChoice: false,
                openAnswers: true
              },
              {
                title: "Q2",
                type: TEXT
              }
            ]
          }) {
            id
          }
        }
      `

      let data = await rp({
        method: 'POST',
        body: {
          query: mutation
        },
        uri: 'http://localhost:4000',
        json: true,
        simple: true,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Token token="abcdef",username="messam"'
        }
      })

      expect(postOptions.body.title).to.eql('magic')
      expect(postOptions.body.tag_names).to.eql(['habalola', '2olla'])
      expect(postOptions.body.user_id).to.eql(20)
      expect(postOptions.body.questions.length).to.eql(2)
      expect(postOptions.body.questions[0].text).to.eql('Q1')
      expect(postOptions.body.questions[0].type).to.eql('McqQuestion')
      expect(postOptions.body.questions[0].options).to.eql(['A1', 'A2'])
      expect(postOptions.body.questions[0].multi_answers).to.eql(false)
      expect(postOptions.body.questions[0].user_can_add).to.eql(true)
      expect(postOptions.body.questions[1].text).to.eql('Q2')
      expect(postOptions.body.questions[1].type).to.eql('TextQuestion')

      expect(getOptions.qs.username).to.eql('messam')
      expect(getOptions.qs.token).to.eql('abcdef')
    })
  })

  describe('createAnswer', function() {
    it('creates answer for post', async function () {
      var postStub = this.sandbox.stub(rp, 'post')
      let postOptions = undefined
      let getOptions = undefined

      postStub.callsFake(function(options) {
        postOptions = options
        return new Promise(function(resolve, reject) {
          resolve({
            statusCode: 200,
            body: {
              id: 20,
              email: 'messam@this.com',
              username: 'messam',
              token: 'abcdef'
            }
          })
        })
      })

      var getStub = this.sandbox.stub(rp, 'get')

      getStub.callsFake(function(options) {
        getOptions = options
        return new Promise(function(resolve, reject) {
          resolve({
            statusCode: 200,
            body: {
              id: 20,
              email: 'messam@this.com',
              username: 'messam',
              token: 'abcdef'
            }
          })
        })
      })

      mutation = `
        mutation {
          createAnswer(postId: 20, answer: [
            {
              questionId: 51,
              optionIds: [200]
            },
            {
              questionId: 52,
              text: "7zmbola"
            }
          ])
        }
      `

      let data = await rp({
        method: 'POST',
        body: {
          query: mutation
        },
        uri: 'http://localhost:4000',
        json: true,
        simple: true,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Token token="abcdef",username="messam"'
        }
      })

      expect(postOptions.body.post_id).to.eql(20)
      expect(postOptions.body.user_id).to.eql(20)
      expect(postOptions.body.question_answers.length).to.eql(2)
      expect(postOptions.body.question_answers[0].question_id).to.eql(51)
      expect(postOptions.body.question_answers[0].option_ids).to.eql([200])
      expect(postOptions.body.question_answers[1].question_id).to.eql(52)
      expect(postOptions.body.question_answers[1].text).to.eql('7zmbola')

      expect(getOptions.qs.username).to.eql('messam')
      expect(getOptions.qs.token).to.eql('abcdef')
    })
  })

  describe('addOption', function() {
    it('adds option for question', async function () {
      var postStub = this.sandbox.stub(rp, 'post')
      let postOptions = undefined
      let getOptions = undefined

      postStub.callsFake(function(options) {
        postOptions = options
        return new Promise(function(resolve, reject) {
          resolve({
            statusCode: 200,
            body: {
              id: 20,
              email: 'messam@this.com',
              username: 'messam',
              token: 'abcdef'
            }
          })
        })
      })

      var getStub = this.sandbox.stub(rp, 'get')

      getStub.callsFake(function(options) {
        getOptions = options
        return new Promise(function(resolve, reject) {
          resolve({
            statusCode: 200,
            body: {}
          })
        })
      })

      mutation = `
        mutation {
          addOption(questionId: 20, option: "7alambo7a") {
            id
          }
        }
      `

      let data = await rp({
        method: 'POST',
        body: {
          query: mutation
        },
        uri: 'http://localhost:4000',
        json: true,
        simple: true,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Token token="abcdef",username="messam"'
        }
      })

      expect(postOptions.body.question_id).to.eql(20)
      expect(postOptions.body.option).to.eql('7alambo7a')

      expect(getOptions.qs.username).to.eql('messam')
      expect(getOptions.qs.token).to.eql('abcdef')
    })
  })
})
