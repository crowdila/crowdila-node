const { ApolloServer, gql, AuthenticationError } = require('apollo-server');

// load type defs from schema
const fs = require('fs');
const typeDefs = fs.readFileSync('schema.graphql', 'utf8')

// load resolvers
const resolvers = require('./schema')

const context = function({ req }) {
  var auth = req.headers.authorization;
  if (auth === undefined) {
    return {}
  }
  var authType = auth.split(' ')[0];
  if (authType !== 'Token') {
    throw new AuthenticationError('Wrong auth type')
  }

  var authString = auth.split(' ').slice(1).join('');
  var maps = authString.split(',').map((e) => (e.split('='))).map((e) => ({ [e[0]]: e[1].split('"').join('') }));
  var authMap = {};
  maps.forEach(function(map) {
    authMap = Object.assign(map, authMap)
  });

  if(authMap.username === undefined || authMap.token === undefined) {
    throw new AuthenticationError('Missing auth params')
  }

  return authMap
}

const server = new ApolloServer({ typeDefs, resolvers, context });

module.exports = server

server.listen().then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});
