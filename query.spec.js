const rp = require('request-promise');

const expect = require('chai').expect
const sinon = require('sinon');
const Promise = require('bluebird')

describe('Query Object', function () {
  describe('me', function() {
    it('returns current user', async function () {
      var stub = this.sandbox.stub(rp, 'get')
      let getOptions = undefined

      stub.callsFake(function(options) {
        getOptions = options
        return new Promise(function(resolve, reject) {
          resolve({
            statusCode: 200,
            body: {
              id: 20,
              email: 'messam@this.com',
              username: 'messam'
            }
          })
        })
      })

      let data = await rp({
        method: 'GET',
        qs: {
          query: '{me {email,username}}'
        },
        uri: 'http://localhost:4000',
        json: true,
        simple: true,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Token token="nyancat",username="messam"'
        }
      })

      expect(getOptions.qs.username).to.eql('messam')
      expect(getOptions.qs.token).to.eql('nyancat')

      expect(data.data.me.email).to.eql('messam@this.com')
      expect(data.data.me.username).to.eql('messam')
    })
  })

  describe('login', function() {
    it('returns current user', async function () {
      var stub = this.sandbox.stub(rp, 'get')
      let getOptions = undefined

      stub.callsFake(function(options) {
        getOptions = options
        return new Promise(function(resolve, reject) {
          resolve({
            statusCode: 200,
            body: {
              id: 20,
              email: 'messam@this.com',
              username: 'messam',
              token: 'abcdef'
            }
          })
        })
      })

      let data = await rp({
        method: 'GET',
        qs: {
          query: '{login(username: "messam", password: "nyancat") {email,username,token}}'
        },
        uri: 'http://localhost:4000',
        json: true,
        simple: true,
        headers: {
          'Content-Type': 'application/json'
        }
      })

      expect(getOptions.qs.username).to.eql('messam')
      expect(getOptions.qs.password).to.eql('nyancat')

      expect(data.data.login.email).to.eql('messam@this.com')
      expect(data.data.login.username).to.eql('messam')
      expect(data.data.login.token).to.eql('abcdef')
    })
  })

  describe('post', function() {
    it('should get post', async function() {
      var stub = this.sandbox.stub(rp, 'get')
      let getOptions = undefined

      stub.callsFake(function(options) {
        getOptions = options
        return new Promise(function(resolve, reject) {
          resolve({
            statusCode: 200,
            body: [
              {
                id: 1,
                title: 'oshiete, oshiete yo, sono shikumi wo'
              }
            ]
          })
        })
      })

      let data = await rp({
        method: 'GET',
        qs: {
          query: '{post(id: 1) {id, title}}'
        },
        uri: 'http://localhost:4000',
        json: true,
        simple: true,
        headers: {
          'Content-Type': 'application/json'
        }
      })

      expect(getOptions.qs.ids).to.eql([1])

      expect(data.data.post.id).to.eql(1)
      expect(data.data.post.title).to.eql('oshiete, oshiete yo, sono shikumi wo')
    })
  })

  describe('searchPosts', function() {
    it('should get posts', async function() {
      var stub = this.sandbox.stub(rp, 'get')
      let getOptions = undefined

      stub.callsFake(function(options) {
        getOptions = options
        return new Promise(function(resolve, reject) {
          resolve({
            statusCode: 200,
            body: [
              {
                id: 1,
                title: 'oshiete, oshiete yo, sono shikumi wo'
              }
            ]
          })
        })
      })

      let data = await rp({
        method: 'GET',
        qs: {
          query: '{searchPosts(title: "7amada") {id, title}}'
        },
        uri: 'http://localhost:4000',
        json: true,
        simple: true,
        headers: {
          'Content-Type': 'application/json'
        }
      })
      expect(getOptions.qs.title).to.eql('7amada')
      expect(getOptions.qs.limit).to.eql(20)
      expect(getOptions.qs.id_from).to.eql(0)

      expect(data.data.searchPosts[0].id).to.eql(1)
      expect(data.data.searchPosts[0].title).to.eql('oshiete, oshiete yo, sono shikumi wo')
    })
  })

  describe('filterByTags', function() {
    it('should get posts', async function() {
      var stub = this.sandbox.stub(rp, 'get')
      let getOptions = undefined

      stub.callsFake(function(options) {
        getOptions = options
        return new Promise(function(resolve, reject) {
          resolve({
            statusCode: 200,
            body: [
              {
                id: 1,
                title: 'oshiete, oshiete yo, sono shikumi wo'
              }
            ]
          })
        })
      })

      let data = await rp({
        method: 'GET',
        qs: {
          query: '{filterByTags(tags: ["7amada"]) {id, title}}'
        },
        uri: 'http://localhost:4000',
        json: true,
        simple: true,
        headers: {
          'Content-Type': 'application/json'
        }
      })

      expect(getOptions.qs.tag_names).to.eql(['7amada'])
      expect(getOptions.qs.limit).to.eql(20)
      expect(getOptions.qs.id_from).to.eql(0)

      expect(data.data.filterByTags[0].id).to.eql(1)
      expect(data.data.filterByTags[0].title).to.eql('oshiete, oshiete yo, sono shikumi wo')
    })
  })

  describe('tags', function() {
    it('should get tags', async function() {
      var stub = this.sandbox.stub(rp, 'get')

      stub.callsFake(function(options) {
        return new Promise(function(resolve, reject) {
          resolve({
            statusCode: 200,
            body: [
              {
                name: 'oshiete'
              },
              {
                name: 'shikumi'
              }
            ]
          })
        })
      })

      let data = await rp({
        method: 'GET',
        qs: {
          query: '{tags {name}}'
        },
        uri: 'http://localhost:4000',
        json: true,
        simple: true,
        headers: {
          'Content-Type': 'application/json'
        }
      })

      expect(data.data.tags[0].name).to.eql('oshiete')
      expect(data.data.tags[1].name).to.eql('shikumi')
    })
  })

  describe('avatars', function() {
    it('should get avatars', async function() {
      var stub = this.sandbox.stub(rp, 'get')

      stub.callsFake(function(options) {
        return new Promise(function(resolve, reject) {
          resolve({
            statusCode: 200,
            body: [
              {
                id: 0,
                avatar_url: 'oshiete'
              },
              {
                id: 1,
                avatar_url: 'shikumi'
              }
            ]
          })
        })
      })

      let data = await rp({
        method: 'GET',
        qs: {
          query: '{avatars {id, url}}'
        },
        uri: 'http://localhost:4000',
        json: true,
        simple: true,
        headers: {
          'Content-Type': 'application/json'
        }
      })

      expect(data.data.avatars[0].id).to.eql(0)
      expect(data.data.avatars[0].url).to.eql('oshiete')
      expect(data.data.avatars[1].id).to.eql(1)
      expect(data.data.avatars[1].url).to.eql('shikumi')
    })
  })
})
