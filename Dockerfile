FROM node:carbon

ADD . .

RUN npm install

CMD ["node", "index.js"]

